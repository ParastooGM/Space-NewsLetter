const request = require("supertest");
const { expect } = require("expect");
const { app } = require("../app");

describe("Testing POST /signup.html.", () => {
  it("respond with valid HTTP status code.", async () => {
    const response = await request(app).post("/signup.html").send({
      firstName: "firstname",
      lastName: "lastname",
      email: "firstname@lastname.com",
    });

    expect(response.status).toBe(200);
  });
});

describe("Testing GET / .", () => {
  it("respond with valid HTTP status code.", async () => {
    const response = await request(app).get("/");
    expect(response.status).toBe(200);
  });
});
